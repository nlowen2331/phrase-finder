const { parse } =require('aws-multipart-parser')

exports.handler = async (event) => {
    
    console.log(event)
    
    //Handle CORS response
  
    if(event.requestContext.http.method==='OPTIONS'){
      return({
        statusCode: 200,
        headers:{
           'Content-Type': 'multipart/form-data',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Authorization,Content-Type',
            'Access-Control-Allow-Method': 'GET,POST,OPTIONS'
        }
      })
    }

    //Parse form-data -> JSON
    let FormData = parse(event,true)
    
    if(!FormData.phrase){
        return({
            statusCode: 422,
            body: JSON.stringify({message:`Missing 'phrase' field`})
        })
    }
    
    if(!FormData.text){
        return({
            statusCode: 422,
            body: JSON.stringify({message:`Missing file 'text'`})
        })
    }
    
    //Convert Buffer to String
    const Text = FormData.text.content.toString()
    
    //Attempt to run algorithm with given fields
    let ResponseArray 
    let AlgoError=null
    try{
        ResponseArray= await SearchAlgorithm(FormData.phrase,Text,FormData.limit)
    }
    catch(err){
        AlgoError=err
    }

    if(AlgoError){
        console.log(AlgoError)
        return({
            statusCode: 500,
            body: JSON.stringify({message:'An unexpected error occured'})
        })
    }
    

    return({
        statusCode: 200,
        body: JSON.stringify({snippets: ResponseArray,message: 'OK',hits: ResponseArray.length})
    })
    
    
};


//Takes in a searchPhrase, text, and options {charStart,limit}
//Returns an array of snippets from text that include given searchPhrases
const SearchAlgorithm = async (searchPhrase,text,limit)=>{
    
    //Form data returns 'undefined' literally for undefined properties
    
    if(!limit||limit==='undefined'){
        limit=100
    }
    
    //Use RegExp to catch snippets
    
    let PhraseExpression = new RegExp('[^.?!,]*(?<=[.?\\s!,])'+searchPhrase+'(?=[\\s.?!])[^.?!]*[.?!]','gim')
    
    let Snippet
    let Snippets = []

    do {
		Snippet = PhraseExpression.exec(text)
		//If match, push snippet into array to return
		if(Snippet){
		    //replace first space and all new lines
			let noSpaceSnippet = Snippet[0].replace(/\s/,'')
	        let finalSnippet = noSpaceSnippet.replace(/[\n\r]*/g,'')
			Snippets.push({snippet: finalSnippet, indexOf: PhraseExpression.lastIndex-Snippet[0].length})
		}
	} while(Snippet&&limit>Snippets.length)

    return Snippets

}